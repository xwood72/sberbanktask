package com.example.xw.itseemstherainisstarting.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by xw on 21.07.17.
 */

//{"cod":"404","message":"city not found"}
public class ErrorResponse {

    @SerializedName("cod")
    private String cod;

    @SerializedName("message")
    private String message;

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
