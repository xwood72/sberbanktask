package com.example.xw.itseemstherainisstarting.tasks;

import android.location.Location;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.xw.itseemstherainisstarting.models.CurrentWeatherResponse;
import com.example.xw.itseemstherainisstarting.models.ErrorResponse;
import com.example.xw.itseemstherainisstarting.network.WeatherClient;
import com.example.xw.itseemstherainisstarting.ui.activities.IGetWeatherCallback;
import com.google.gson.Gson;

import java.io.IOException;

/**
 * Created by xw on 21.07.17.
 */

public class GetWeatherTask extends AsyncTask<Object, Void, Object> {

    private IGetWeatherCallback callback;

    public GetWeatherTask(IGetWeatherCallback callback) {
        this.callback = callback;
    }

    @Override
    protected Object doInBackground(Object... params) {
        if (params[0] == null) return null;
        String cityName = null;
        Location loc = null;

        String response = null;
        try {
            response = null;
            if (params[0] instanceof String){
                cityName = (String) params[0];
                response = WeatherClient.getWeatherByCity(cityName);
            } else if (params[0] instanceof Location){
                loc = (Location) params[0];
                response = WeatherClient.getWeatherByCoords(loc);
            }
        } catch (IOException e) {
            e.printStackTrace();

        }

        Object result = parseResponse(response);
        return result;
    }

    @Override
    protected void onPostExecute(Object obj) {
        super.onPostExecute(obj);
        if (obj instanceof CurrentWeatherResponse) {
            callback.onSuccess((CurrentWeatherResponse) obj);
        } else if (obj instanceof ErrorResponse){
            callback.onFail((ErrorResponse) obj);
        }
    }

    private Object parseResponse(String response){
        if (response == null) return null;
        Gson gson = new Gson();
        Object result = null;
        if (response.contains("\"cod\":404")){
            result = gson.fromJson(response, ErrorResponse.class);
        } else {
            result = gson.fromJson(response, CurrentWeatherResponse.class);
        }
        return result;
    }
}
