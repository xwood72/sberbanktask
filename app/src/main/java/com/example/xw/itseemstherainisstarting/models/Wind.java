package com.example.xw.itseemstherainisstarting.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by xw on 21.07.17.
 */


//     "speed":0.96,
//     "deg":285.001
public class Wind {

    @SerializedName("speed")
    private Double speed;

    @SerializedName("deg")
    private Double deg;

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getDeg() {
        return deg;
    }

    public void setDeg(Double deg) {
        this.deg = deg;
    }
}
