package com.example.xw.itseemstherainisstarting.network;

import android.location.Location;
import android.util.Log;

import com.example.xw.itseemstherainisstarting.models.CurrentWeatherResponse;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import static com.example.xw.itseemstherainisstarting.Config.API_KEY;
import static com.example.xw.itseemstherainisstarting.Config.BASE_URL;

/**
 * Created by xw on 21.07.17.
 */

public class WeatherClient {

    public static String getWeatherByCity(String cityName) throws IOException{
        if (cityName == null && cityName.equals("")) return null;
        URL url = makeUrl(cityName);
        StringBuilder sbResponse = new StringBuilder();
        URLConnection urlConnection = (HttpURLConnection) url.openConnection();
        InputStream in = urlConnection.getInputStream();
        InputStreamReader isw = new InputStreamReader(in);
        BufferedReader r = new BufferedReader(isw);
        String line;
        while ((line = r.readLine()) != null) {
            sbResponse.append(line).append('\n');
        }
        Log.d("RESPONSE (by city name)", sbResponse.toString());
        return sbResponse.toString();
    }

    public static String getWeatherByCoords(Location location) throws IOException{
        if (location == null) return null;
        URL url = makeUrl(location);
        StringBuilder sbResponse = new StringBuilder();
        URLConnection urlConnection = (HttpURLConnection) url.openConnection();
        InputStream in = urlConnection.getInputStream();;
        InputStreamReader isw = new InputStreamReader(in);
        BufferedReader r = new BufferedReader(isw);
        String line;
        while ((line = r.readLine()) != null) {
            sbResponse.append(line).append('\n');
        }
        Log.d("RESPONSE (by coords)", sbResponse.toString());
        return sbResponse.toString();
    }

    //URL Example
    //http://api.openweathermap.org/data/2.5/weather?q=Kazan,ru&appid=83dd00e458507567178f3f7e13874bf5
    private static URL makeUrl(String cityName){
        StringBuilder sbUrl = new StringBuilder();
        sbUrl.append(BASE_URL)
                .append("q=")
                .append(cityName)
                .append("&appid=")
                .append(API_KEY);
        URL url = null;
        try {
            url = new URL(sbUrl.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }

    //URL Example
    //http://api.openweathermap.org/data/2.5/weather?lat=55.79&lon=49.12&appid=83dd00e458507567178f3f7e13874bf5
    private static URL makeUrl(Location location){
        StringBuilder sbUrl = new StringBuilder();
        sbUrl.append(BASE_URL)
                .append("lat=")
                .append(location.getLatitude())
                .append("&lon=")
                .append(location.getLongitude())
                .append("&appid=")
                .append(API_KEY);
        URL url = null;
        try {
            url = new URL(sbUrl.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }
}
