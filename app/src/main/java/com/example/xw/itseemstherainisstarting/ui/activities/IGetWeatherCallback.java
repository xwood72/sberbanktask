package com.example.xw.itseemstherainisstarting.ui.activities;

import com.example.xw.itseemstherainisstarting.models.CurrentWeatherResponse;
import com.example.xw.itseemstherainisstarting.models.ErrorResponse;

/**
 * Created by xw on 21.07.17.
 */

public interface IGetWeatherCallback {

    CurrentWeatherResponse onSuccess(CurrentWeatherResponse response);

    ErrorResponse onFail(ErrorResponse response);
}
