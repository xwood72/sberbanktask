package com.example.xw.itseemstherainisstarting.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by xw on 21.07.17.
 */


public class Coords {

    @SerializedName("lon")
    private Double lon;

    @SerializedName("lat")
    private Double lat;

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

}
