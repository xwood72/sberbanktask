package com.example.xw.itseemstherainisstarting.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by xw on 21.07.17.
 */

public class Main {


//    "temp":285.68,
//            "humidity":74,
//            "pressure":1016.8,
//            "temp_min":284.82,
//            "temp_max":286.48

    @SerializedName("temp")
    private Double temp;

    @SerializedName("humidity")
    private Integer humidity;

    @SerializedName("pressure")
    private Double pressure;

    @SerializedName("temp_min")
    private Double temp_min;

    @SerializedName("temp_max")
    private Double temp_max;

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Double getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(Double temp_min) {
        this.temp_min = temp_min;
    }

    public Double getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(Double temp_max) {
        this.temp_max = temp_max;
    }
}
