package com.example.xw.itseemstherainisstarting.ui.activities;

import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xw.itseemstherainisstarting.R;
import com.example.xw.itseemstherainisstarting.models.CurrentWeatherResponse;
import com.example.xw.itseemstherainisstarting.models.ErrorResponse;
import com.example.xw.itseemstherainisstarting.tasks.GetWeatherTask;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class CurrentWeatherInfoActivity extends AppCompatActivity
    implements IGetWeatherCallback {

    private FusedLocationProviderClient mLocationClient;

    private EditText etCityName;
    private Button btnGetWeatherByCoords;
    private Button btnFindByCity;

    private TextView tvWindSpeed;
    private TextView tvPressure;
    private TextView tvHumidity;
    private TextView tvTemp;
    private TextView tvTownName;

    private LocationManager locManager;
    private Double curLon = null;
    private Double curLat = null;

    private Location curLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_weather_info);

        initUi();
        getLastLocation();
        setListeners();

    }

    private void initUi() {
        mLocationClient = getFusedLocationProviderClient(this);
        etCityName = (EditText) findViewById(R.id.etTownName);
        btnGetWeatherByCoords = (Button) findViewById(R.id.btnWeatherByCoords);
        btnFindByCity = (Button) findViewById(R.id.btnFindByCityName);

        tvWindSpeed = (TextView) findViewById(R.id.tvWindSpeed);
        tvPressure = (TextView) findViewById(R.id.tvPressure);
        tvHumidity = (TextView) findViewById(R.id.tvHumidity);
        tvTemp = (TextView) findViewById(R.id.tvTemp);
        tvTownName = (TextView) findViewById(R.id.tvTownName);
    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

        }
        FusedLocationProviderClient locationClient = getFusedLocationProviderClient(this);
        locationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // GPS location can be null if GPS is switched off
                        if (location != null) {
                            curLat = location.getLatitude();
                            curLon = location.getLongitude();
                            curLocation = location;
                            Toast.makeText(CurrentWeatherInfoActivity.this,
                                    "Current lat: " + curLat + " lon: " + curLon,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("DETECT LOCATION", "Error trying to get last GPS location");
                        e.printStackTrace();
                    }
                });
    }

    private void setListeners() {
        btnFindByCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cityName = etCityName.getText().toString();
                GetWeatherTask task = new GetWeatherTask(CurrentWeatherInfoActivity.this);
                task.execute(cityName);
            }
        });

        btnGetWeatherByCoords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (curLocation != null){
                    GetWeatherTask task = new GetWeatherTask(CurrentWeatherInfoActivity.this);
                    task.execute(curLocation);
                } else {
                    Toast.makeText(CurrentWeatherInfoActivity.this,
                            "Can't detect current location", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    public CurrentWeatherResponse onSuccess(CurrentWeatherResponse response) {

        Toast.makeText(this, response.getName(), Toast.LENGTH_SHORT).show();
        tvWindSpeed.setText("Wind speed: " + response.getWind().getSpeed());
        tvPressure.setText("Pressure: " + response.getMain().getPressure());
        tvHumidity.setText("Humidity: " + response.getMain().getHumidity());
        tvTemp.setText("Temperature: " + response.getMain().getTemp());
        tvTownName.setText("City name: " + response.getName());
        return null;
    }

    @Override
    public ErrorResponse onFail(ErrorResponse response) {
        Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
        return null;
    }
}
